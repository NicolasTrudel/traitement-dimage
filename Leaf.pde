class Leaf {
  PVector pos;
  boolean reached = false;

  Leaf(float x, float y, float z) {
   // pos = new PVector(random(x-random(100,150),x+random(100,150)), random(y-300,y-100), random(z-150,z+150));
   pos = new PVector(random(x-random(100,150),x+random(100,150)), random(y-150,y+150), random(z+100,z+200));
   
    //pos.mult(random(width/2));
   //pos.y -= height/4;
  }

  void reached() {
    reached = true;
  }

  void show() {
    fill(255,0,0);
    noStroke();
    pushMatrix();
    translate(pos.x, pos.y, pos.z);
    //sphere(4);
    ellipse(0,0, 8, 8);
    popMatrix();
  }
  
  
}
