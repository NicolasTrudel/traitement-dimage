//== ©Copyright to Nicolas Trudel || All Rights Reserved ==//
import peasy.*;

int cols,rows;
int scl = 20;
int w = 1500;
int h = 1500;

boolean move = true;

String direction = "front";


boolean showAxis = false;


float min_dist = 5;
float max_dist = 200;
float flying = 0;

Tree mainTree;


float[][] terrain;

color brown = color(139,69,19);
color green = color(0,50,0);
  
PeasyCam cam;
  
  
void showAxis()
{
   strokeWeight(10);
  
  
  //X AXE  RED
  stroke(255,0,0);
  line(0,0,0 ,5000,0,0);
  
  //Y AXE  GREEN
  stroke(0,255,0);
  line(0,0,0 ,0,5000,0);
  
  //Z AXE  BLUE
  stroke(0,0,255);
  line(0,0,0 ,0,0,5000);
   
}
  
  

void setup() {
  frameRate(60);
    
   mainTree = new Tree(cols / 2, rows / 2, 0);
 
   cam = new PeasyCam(this, 1000);
   
   
  size(1366, 768, P3D);
  
 
  cols = w / scl;
  rows = h / scl;
  terrain = new float[cols][rows];
  cam.lookAt(w/2,h/2,100);
  cam.rotateX(5.25*PI/3);
 
}



void draw() {
    
  
   if(move == true && direction == "front")
   {
     flying -= 0.05;
   }else if(move == true && direction == "back")
   {
     flying += 0.05;
   }
   
   
    
    
    
    float yoff = flying;
   for (int y = 0; y < rows; y++) {
   float xoff = 0;
    for(int x = 0; x < cols; x++) {
      terrain[x][y] = map(noise(xoff,yoff), 0, 1, -110, 110);
      xoff += 0.05;
    }
    yoff += 0.05;
   }
 
 
  
  background(0,114,163);
  stroke(0);
  
  translate(width/2,height/2+50);

 

  
  
 
  
  translate(-w/2, -h/2);
  
    if(mainTree.doneGrowing == 0)
    {
    mainTree.grow();
    }
  
    
  for (int y = 0; y < rows-1; y++) {
    strokeWeight(1);
    stroke(0);
    beginShape(TRIANGLE_STRIP);
    for(int x = 0; x < cols; x++) {
    
        if(terrain[x][y] <= 5 && terrain[x][y] > -35)
        {
          
           fill(brown);
           vertex(x*scl, y*scl, terrain[x][y]);
          vertex(x*scl, (y+1)*scl, terrain[x][y+1]);
        }else if(terrain[x][y] <= -35)
        {
         
          fill(0,0,255);
          vertex(x*scl, y*scl, -35);
          vertex(x*scl, (y+1)*scl, -35);
          
        }else if(terrain[x][y] > 5 && terrain[x][y] < 75)
        {
          
          fill(green);
          vertex(x*scl, y*scl, terrain[x][y]);
          vertex(x*scl, (y+1)*scl, terrain[x][y+1]);
           
        }
        if(terrain[x][y] > 75)
        {
          fill(255);
          
          vertex(x*scl, y*scl, terrain[x][y]);
          vertex(x*scl, (y+1)*scl, terrain[x][y+1]);
            
        }
       
    }
    endShape(CLOSE);
  }
  
  for (int y = 0; y < rows-1; y++)
  {
    strokeWeight(1);
    stroke(0);
    for(int x = 0; x < cols; x++)
    {
       if(terrain[x][y] > 75.0)
        {
          if(mainTree.doneGrowing == 1)
          {
              mainTree.show(x*scl, y*scl, terrain[x][y]);
              
          }
         
        }
    }
  }
  
  
  
  

 if(showAxis){showAxis();}

  
}




void keyPressed()
{
  if(key == ' ' && move == false)
  {
    move = true;
  }else if(key == ' ' && move == true)
  {
    move = false;
  }
  
  if(key == 's')
  {
    direction = "back";
  }else if(key == 'w')
  {
    direction = "front";
  }else if(key == 'g')
  {
    if(showAxis)
    {
      showAxis = false;
    }else
    {
      showAxis = true;
    }
    
   
  }
  
  
  
}
